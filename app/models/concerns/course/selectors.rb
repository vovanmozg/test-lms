module Course::Selectors
  def all_by_user(user_id)
    # @todo join with promo_codes
    Course
        .joins(course_appointments: :user)
        .where('course_appointments.user_id': user_id)
  end
end