class Course < ApplicationRecord
  extend Selectors

  belongs_to :category
  has_many :lessons
  has_many :course_appointments
  has_many :promo_codes

end
