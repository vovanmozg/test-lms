class CourseController < ApplicationController
  def by_user
    @courses = Course.all_by_user(params[:_id])

    render json: @courses.as_json(only: [:id, :title])
  end
end
