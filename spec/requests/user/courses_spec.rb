require 'rails_helper'

RSpec.describe 'UserCourses', type: :request do
  include ApiHelpers

  let(:category) { create(:category) }

  it 'returns accessible courses' do

    user = create(:user)

    course = create(:course)

    pc = create(:promo_code, course: course)

    ca = create(:course_appointment, course: course, user: user)

    upc = create(:user_promo_code, promo_code: pc, user: user)


    # inaccessible courses
    create_list(:course, 5, category: category)
    users = create_list(:user, 3)

    # appoint 2 courses to user
    upc = create(:user_promo_code)
    appointments = [
        create(:course_appointment, user: upc.user, course: upc.promo_code.course),
    ]

    # appoint course to other user
    create(:course_appointment, user: users.first, course: appointments[0].course)

    get "/api/users/#{upc.user.id}/courses"
    expected = appointments.map(&:course).map { |c| c.slice([:id, :title]) }
    expect(json_response).to eq(expected)
  end

  it 'does not return overdue courses' do
    user = create(:user)
    course = create(:course)
    pc = create(:promo_code, course: course)
    upc = create(:user_promo_code, promo_code: pc, user: user, expiration_date: Time.now - 1.day)
    create(:course_appointment, course: course, user: user)
    get "/api/users/#{upc.user.id}/courses"
    expect(json_response).to eq([])
  end
end
