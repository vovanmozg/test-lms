FactoryBot.define do
  factory :promo_code do
    course
    code { (0...8).map { (0...9).to_a[rand(10)] }.join }
    lessons_count { 3 }
    duration { 7 }
    reusable { false }
    active { true }
  end
end
