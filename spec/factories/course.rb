FactoryBot.define do
  factory :course do
    title { FFaker::Lorem.sentence(3) }
    category

    trait :with_lessons do
      after(:create) do |course|
        3.times { create(:lesson, course: course) }
      end
    end
  end
end
