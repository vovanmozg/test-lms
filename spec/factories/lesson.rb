FactoryBot.define do
  factory :lesson do
    title { FFaker::Lorem.sentence(3) }
    content { FFaker::Lorem.sentences(5) }
    course
  end
end
