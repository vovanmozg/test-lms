FactoryBot.define do
  factory :user_promo_code do
    promo_code
    user
    active { true }
    expiration_date { Time.now + 7.days }
  end
end
