class CreateUserPromoCodes < ActiveRecord::Migration[5.2]
  def change
    create_table :user_promo_codes do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :promo_code, foreign_key: true
      t.boolean :active
      t.date :expiration_date

      t.timestamps
    end
  end
end
