class CreatePrices < ActiveRecord::Migration[5.2]
  def change
    create_table :prices do |t|
      t.float :price
      t.belongs_to :course, foreign_key: true

      t.timestamps
    end
  end
end
