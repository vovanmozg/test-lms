class CreatePromoCodes < ActiveRecord::Migration[5.2]
  def change
    create_table :promo_codes do |t|
      t.belongs_to :course, foreign_key: true
      t.string :code
      t.integer :lessons_count
      t.integer :duration
      t.boolean :reusable
      t.boolean :active

      t.timestamps
    end
  end
end
