class CreateLessonProgresses < ActiveRecord::Migration[5.2]
  def change
    create_table :lesson_progresses do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :lesson, foreign_key: true
      t.boolean :finished

      t.timestamps
    end
  end
end
