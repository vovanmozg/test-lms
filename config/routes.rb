Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #get 'by_user/:id', to: 'course#by_user'

  #namespace :api, defaults: { format: :json } do
  scope '/api' do
    # list of resources
    scope '/users' do
      get '/:_id/courses' => 'course#by_user'
      get '/:_id/lessons' => 'lesson#by_user'
    end
  end
end
